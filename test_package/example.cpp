#include "myhtml/myhtml.h"
#include "myhtml/serialization.h"

int main()
{
    auto* myhtml = myhtml_create();
    myhtml_init(myhtml, MyHTML_OPTIONS_DEFAULT, 1, 0);

    auto* tree = myhtml_tree_create();
    myhtml_tree_init(tree, myhtml);

    myhtml_tree_destroy(tree);
    myhtml_destroy(myhtml);

    return 0;
}
