from conans import ConanFile, CMake, tools
import shutil
import os

class MyhtmlConan(ConanFile):
    name        = "myhtml"
    version     = "4.0.5"
    license     = "LGPL 2.1"
    url         = "https: //bitbucket.org/toge/conan-myhtml/"
    description = "Fast C/C++ HTML 5 Parser. Using threads."
    settings    = "os"  , "compiler" , "build_type" , "arch"
    generators  = "cmake"

    def source(self):
        zip_name = "myhtml-{}.zip".format(self.version) 
        tools.download("https://github.com/lexborisov/myhtml/archive/v{}.zip".format(self.version), zip_name)
        tools.unzip(zip_name)
        shutil.move("myhtml-" + self.version, "myhtml")
        os.unlink(zip_name)

    def build(self):
        cmake = CMake(self)
        cmake.definitions["MyHTML_BUILD_SHARED"] = "OFF"
        cmake.configure(source_folder="myhtml")
        cmake.build()

    def package(self):
        self.copy("*.h",     src="myhtml/include", dst="include")
        self.copy("*.lib",   dst="lib",            keep_path=False)
        self.copy("*.dll",   dst="bin",            keep_path=False)
        self.copy("*.so",    dst="lib",            keep_path=False)
        self.copy("*.dylib", dst="lib",            keep_path=False)
        self.copy("*.a",     dst="lib",            keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["myhtml_static", "pthread"]
